// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use std::{
    fs::File,
    io::{self, Write},
    os::unix::io::FromRawFd,
    path::PathBuf,
    process::Command,
    str,
};

use crate::asgen_config::AsgenConfig;

struct RepreproCtrl {
    fd: File,
}

impl RepreproCtrl {
    fn new() -> Self {
        Self {
            fd: unsafe { File::from_raw_fd(3) },
        }
    }

    fn add_file(&mut self, file: &str) {
        writeln!(self.fd, "{}", file)
            .expect("Failed to communicate with the parent reprepro process");
    }
}

fn refresh_release_file(config: &AsgenConfig, suite: &str, component: &str) {
    let mut ctrl = RepreproCtrl::new();
    for arch in &config.suites[suite].architectures {
        ctrl.add_file(&format!("{}/dep11/Components-{}.yml", component, arch));
        ctrl.add_file(&format!("{}/dep11/Components-{}.yml.xz", component, arch));
        ctrl.add_file(&format!("{}/dep11/Components-{}.yml.gz", component, arch));
        ctrl.add_file(&format!("{}/dep11/CID-Index-{}.json.gz", component, arch));
    }

    ctrl.add_file(&format!("{}/dep11/icons-48x48.tar.gz", component));
    ctrl.add_file(&format!("{}/dep11/icons-48x48@2.tar.gz", component));
    ctrl.add_file(&format!("{}/dep11/icons-64x64.tar.gz", component));
    ctrl.add_file(&format!("{}/dep11/icons-64x64@2.tar.gz", component));
    ctrl.add_file(&format!("{}/dep11/icons-128x128.tar.gz", component));
    ctrl.add_file(&format!("{}/dep11/icons-128x128@2.tar.gz", component))
}

/// apt needs the uncompressed files to exist for some reason, get rid of this if possible
fn decompress_components_data(config: &AsgenConfig, suite: &str, component: &str) {
    let component_appstream_location =
        format!("{}/{}/{}/", config.export_dirs.data, suite, component);

    if PathBuf::from(component_appstream_location).exists() {
        for arch in &config.suites[suite].architectures {
            Command::new("gunzip")
                .current_dir(&format!(
                    "{}/{}/{}/",
                    config.export_dirs.data, suite, component
                ))
                .arg(&format!("Components-{}.yml.gz", arch))
                .arg("--force")
                .spawn()
                .map(|mut p| {
                    p.wait()
                        .expect("Failed to extract components metadata using gunzip")
                })
                .expect("Failed to spawn gunzip process");
        }
    }
}

fn create_symlinks(config: &AsgenConfig, suite: &str) {
    for component in &config.suites[suite].sections {
        let component_appstream_location =
            format!("{}/{}/{}", config.export_dirs.data, suite, component);
        let component_archive_location = format!(
            "{}/dists/{}/{}/dep11",
            config.archive_root, suite, component
        );

        if let Err(e) =
            std::os::unix::fs::symlink(component_appstream_location, component_archive_location)
        {
            match e.kind() {
                io::ErrorKind::AlreadyExists => {
                    // everything is okay
                }
                _ => {
                    println!("Failed to symlink metadata into repository");
                }
            }
        }
    }
}

pub fn run(config: AsgenConfig, suite: &str, component: &str) {
    serde_json::to_writer_pretty(
        File::create("asgen-config.json").expect("Failed to create asgen config file"),
        &config,
    )
    .expect("Failed to serialize asgen config");

    let mut cmd = Command::new("appstream-generator")
        .arg("run")
        .arg(suite)
        .arg(component)
        .spawn()
        .expect("Failed to run appstream-generator");

    if let Err(e) = cmd.wait() {
        eprintln!("Failed to run appstream-generator: {}", e);
    }

    create_symlinks(&config, suite);
    decompress_components_data(&config, suite, component);
    refresh_release_file(&config, suite, component);
}

// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

///
/// Consumes a list and returns a list containing only the unique elements,
/// keeping their order
///
pub fn remove_duplicates<T: Eq>(vec: Vec<T>) -> Vec<T> {
    let mut out = Vec::<T>::new();

    for e in vec {
        if !out.contains(&e) {
            out.push(e);
        }
    }

    out
}

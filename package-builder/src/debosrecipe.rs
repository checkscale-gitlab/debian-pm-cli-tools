// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

extern crate debos_parser;

use std::collections::HashMap;

use debos_parser::{DebosAction, DebosError, DebosRecipe};

use std::path::PathBuf;

fn expand_actions(
    actions: &[DebosAction],
    recipe_path: PathBuf,
    architecture: &str,
) -> Vec<DebosAction> {
    actions
        .iter()
        .flat_map(|a| {
            if let DebosAction::Recipe(r) = a {
                let mut subrecipe_path = recipe_path.clone();
                subrecipe_path.push(&r.recipe);

                let mut variables = r.variables.clone().unwrap_or_default();
                variables.insert("architecture".to_owned(), architecture.to_owned());
                let subrecipe = DebosRecipe::from_template_file(&subrecipe_path, variables);

                match subrecipe {
                    Ok(recipe) => recipe.actions().clone(),
                    Err(e) => {
                        eprintln!(
                            "Warning: Failed to parse {}: {:?}",
                            subrecipe_path.to_string_lossy(),
                            e
                        );
                        vec![a.clone()]
                    }
                }
            } else {
                vec![a.clone()]
            }
        })
        .collect()
}

pub fn expanded_recipe(
    mut path: PathBuf,
    values: HashMap<String, String>,
) -> Result<DebosRecipe, DebosError> {
    let mut recipe = DebosRecipe::from_template_file(&path, values)?;

    // Remove file name to get location
    path.set_file_name(String::new());

    let expanded_actions = expand_actions(recipe.actions(), path, recipe.architecture());
    recipe.set_actions(expanded_actions);

    //println!("{}", recipe.to_yaml().unwrap());

    Ok(recipe)
}

pub fn get_used_packages(recipe: &DebosRecipe) -> Vec<String> {
    recipe
        .actions()
        .iter()
        .filter(|a| matches!(a, DebosAction::Apt(_)))
        .flat_map(|a| {
            if let DebosAction::Apt(apt_action) = a {
                apt_action.packages.clone()
            } else {
                unreachable!();
            }
        })
        .collect()
}

// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

pub fn build_image_for_architecture(architecture: &str, debian_release: &str) -> Option<String> {
    if debian_release.contains("docker_image=") {
        return Some(debian_release.replace("docker_image=", ""));
    }

    match architecture {
        "amd64" => Some(format!("amd64/debian:{}", debian_release)),
        "i386" => Some(format!("i386/debian:{}", debian_release)),
        "armhf" => Some(format!("arm32v7/debian:{}", debian_release)),
        "arm64" => Some(format!("arm64v8/debian:{}", debian_release)),
        _ => {
            println!("Don't know what image to use for {}", architecture);
            None
        }
    }
}

pub fn native_base_image(debian_release: &str) -> String {
    // debian:testing should be available for most common architectures
    // Change later if needed.
    format!("debian:{}", debian_release)
}

#[test]
fn test_build_image() {
    assert_eq!(
        build_image_for_architecture("armhf", "testing"),
        Some("arm32v7/debian:testing".to_owned())
    );
    assert_eq!(
        build_image_for_architecture("armhf", "docker_image=debian:testing"),
        Some("debian:testing".to_owned())
    );
}

// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use std::io;
use std::path::Path;

use crypto::digest::Digest;
use crypto::md5::Md5;
use crypto::sha1::Sha1;
use crypto::sha2::Sha256;

pub enum Algorithm {
    SHA1,
    SHA2256,
    MD5,
}

pub fn hash_file<T>(path: T, algorithm: Algorithm) -> io::Result<String>
where
    T: AsRef<Path>,
{
    match algorithm {
        Algorithm::SHA1 => {
            let mut hash = Sha1::new();
            hash.input(&std::fs::read(path)?);
            Ok(hash.result_str())
        }
        Algorithm::SHA2256 => {
            let mut hash = Sha256::new();
            hash.input(&std::fs::read(path)?);
            Ok(hash.result_str())
        }
        Algorithm::MD5 => {
            let mut hash = Md5::new();
            hash.input(&std::fs::read(path)?);
            Ok(hash.result_str())
        }
    }
}
